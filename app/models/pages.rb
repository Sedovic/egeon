class Page < ActiveRecord::Base
  attr_protected :created_at, :updated_at

  validates :page_id, :presence => true
  validates :title, :presence => true
  validates :contents, :presence => true
end
