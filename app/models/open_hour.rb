class OpenHour < ActiveRecord::Base
  belongs_to :destination
  attr_protected :destination_id
end
