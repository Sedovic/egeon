class Category
  @@categories_hash = YAML.load(File.open("#{Rails.root}/app/categories.yaml").read)

  def self.root
    @@categories ||= @@categories_hash.map { |c| self.load_from_hash(c) }
    @@categories
  end

  def self.load_from_hash(nested_categories)
    children = nested_categories['children'].map { |c| self.load_from_hash(c) }
    Category.new(nested_categories['id'], nested_categories['name'], children)
  end

  def self.find_by_name(name, categories=self.root)
    self.find_with_block { |c| c.name == name }
  end

  def self.find(id, categories=self.root)
    id = id.to_i
    self.find_with_block { |c| c.id == id }
  end

  def self.find_with_block(categories=self.root, &predicate)
    found = categories.find(&predicate)
    if found
      found
    else
      categories.map { |c| self.find_with_block(c.children, &predicate) }.compact.first
    end
  end


  def initialize(id, name, children)
    @id = id
    @name = name
    @parent = nil
    @children = children.dup
    @children.each { |c| c.set_parent! self }
    @children.freeze
  end

  attr_reader :id, :name, :parent, :children

  def root?
    parent.nil?
  end

  def path
    result = []
    c = self
    until c.root?
      result.insert 0, c
      c = c.parent
    end
    result.insert 0, c
    return result
  end

  def top_section
    path.first
  end

  def documents(order_statement=nil)
    model_name = top_section.name.singularize.titleize
    model = Kernel.qualified_const_get model_name
    model.where(:id => document_ids).order(order_statement)
  end

  def document_ids
    document_categories = DocumentCategory.where(:category_id => id)
    document_categories.pluck :document_id
  end

  def self.get_main_categories
    main_categories = []

    root.each do |root_category|
      root_category.children.each do |main_category|
        main_categories << main_category
      end
    end

    return main_categories
  end

  def set_parent!(parent)
    if @parent.nil?
      @parent = parent
    else
      raise "Cannot change a parent that's been set already"
    end
  end

  def to_s
    children_desc = children.map {|c| "#{c.id}:#{c.name}"}.join(', ')
    parent_desc = if parent.present?
      "#{parent.id}:#{parent.name}"
    else
      'n/a'
    end
    "<Category #{id}:#{name}>"
  end

end
