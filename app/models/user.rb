class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :registerable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :name, :password, :password_confirmation, :remember_me

  has_many :destinations, :foreign_key => 'author_id'
  has_many :events, :foreign_key => 'author_id'
  has_many :personalities, :foreign_key => 'author_id'
  has_many :routes, :foreign_key => 'author_id'
  has_many :terms, :foreign_key => 'author_id'

  has_many :collaborations

  def documents
    destinations + events + personalities + routes + terms
  end

  def collaborated_documents
    collaborations.map &:document
  end
end
