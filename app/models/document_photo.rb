class DocumentPhoto < ActiveRecord::Base
  belongs_to :document, :polymorphic => true
  belongs_to :photo
end
