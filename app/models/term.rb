class Term < ActiveRecord::Base
  attr_protected :source_id, :author_id, :created_at, :updated_at

  scope :public, where(:is_draft => false)
  scope :draft, where(:is_draft => true)

  # Each document must have one author and may have one or more coauthors
  belongs_to :author, :class_name => 'User', :foreign_key => 'author_id'
  has_many :collaborations, :as => :document, :dependent => :destroy
  has_many :coauthors, :through => :collaborations, :source => 'user'
  validates :author, :presence => true
  validate :author_cant_be_coauthor

  # Each document may have one or more photos
  has_many :document_photos, :as => :document, :dependent => :destroy
  has_many :photos, :through => :document_photos, :order => :index

  # Documents have categories associated with them
  has_many :document_categories, :as => :document, :dependent => :destroy
  validate :belongs_to_section
  validate :belongs_to_category

  include DocumentsHelper
end
