class AboutController < ApplicationController
  before_filter :authenticate_user!, :except => [:index]

  def index
    about_page = Page.find_by_page_id("about")
    if about_page.present?
      @title = about_page.title
      @contents = about_page.contents
    else
      @title = default_title
      @contents = default_contents
    end
    @breadcrumbs = [Breadcrumb.new(t :about)]
  end

  def edit
    @doc = Page.find_by_page_id("about")
    if @doc.nil?
      @doc = Page.create! :page_id => "about", :title => default_title, :contents => default_contents
    end
  end

  def default_title
    t(:about_page_header_text)
  end

  def default_contents
    '<p>'+ t(:about_page_text_p1) + '</p><p>' + t(:about_page_text_p2) + '</p>'
  end
end
