class XmlSitemapController < ApplicationController
  respond_to :xml
  def index
    @sitemap_urls = SitemapController.get_flat_sitemap_data(request)
  end
end
