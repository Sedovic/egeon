class PagesController < ApplicationController
  before_filter :authenticate_user!

  def update
    # TODO: fix this to work for any page, not just the About one
    doc = Page.find params[:id]
    doc.update_attributes! params[:page]

    flash[:notice] = t(:document_saved)
    redirect_to about_edit_path
  end
end
