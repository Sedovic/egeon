class MapsController < ApplicationController
  def index
  end

  def markers
    # Cache the map markers:
    # NOTE: We need both the count and last updated ID. Either editing
    # a Destination or removing one should invalidate the cache.
    cache_key ="#{Destination.name}-#{Destination.count}-#{Destination.order(:updated_at).last.updated_at.iso8601}/map-markers"
    @egeonMapMarkers = Rails.cache.fetch(cache_key, expires_in: 20.days) do
      Destination.public.to_gmaps4rails
    end
    render :json => @egeonMapMarkers
  end
end
