class SitemapController < ApplicationController
  def index
    @url_data = SitemapController.get_sitemap_data(request)
  end

  class SitemapUrl
    attr_accessor :name, :location, :change_frequency, :priority, :sub_sitemap_urls
    def initialize (name, location, change_frequency, priority)
      @name = name
      @location = location
      @change_frequency = change_frequency
      @priority = priority
      @sub_sitemap_urls = Array.new
    end
  end

  def self.get_sitemap_data(request)
    base_url = "#{request.host_with_port}"
    http_base_url = "http://" + base_url

    main_links_priority = "monthly"
    main_links_freq = 2.0

    root = SitemapUrl.new("Egeon", http_base_url, "monthly", 1.0)

    root.sub_sitemap_urls << generate_controller_sitemap_url("destinations", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("routes", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("events", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("terms", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("personalities", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("maps", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("sitemap", base_url, main_links_freq, main_links_priority)
    root.sub_sitemap_urls << generate_controller_sitemap_url("about", base_url, main_links_freq, main_links_priority)

    return root
  end

  def self.generate_controller_sitemap_url(controller_name, base_url, main_links_freq, main_links_priority)
    link = Rails.application.routes.url_helpers.url_for :controller => controller_name, :action => "index", :host => base_url
    return SitemapUrl.new(I18n::t(controller_name), link, main_links_freq, main_links_priority)
  end

  def self.get_flat_sitemap_data(request)
    urls = self.get_all_as_flat(get_sitemap_data(request))
    return urls
  end

  def self.get_all_as_flat(sitemap_url)
    items = []

    items << sitemap_url

    sitemap_url.sub_sitemap_urls.each do |sub_sitemap_url|
      self.get_all_as_flat(sub_sitemap_url).each do |sub_sub_sitemap_url|
        items << sub_sub_sitemap_url
      end
    end

    return items
  end
end
