# coding: utf-8
class TermsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  def initialize
    super

    @filter_tags = []
    @filter_tags << "A"
    @filter_tags << "B"
    @filter_tags << "C"
    @filter_tags << "Č"
    @filter_tags << "D"
    @filter_tags << "Ď"
    @filter_tags << "E"
    @filter_tags << "F"
    @filter_tags << "G"
    @filter_tags << "H"
    @filter_tags << "I"
    @filter_tags << "J"
    @filter_tags << "K"
    @filter_tags << "L"
    @filter_tags << "M"
    @filter_tags << "N"
    @filter_tags << "Ň"
    @filter_tags << "O"
    @filter_tags << "P"
    @filter_tags << "Q"
    @filter_tags << "R"
    @filter_tags << "Ř"
    @filter_tags << "S"
    @filter_tags << "Š"
    @filter_tags << "T"
    @filter_tags << "Ť"
    @filter_tags << "U"
    @filter_tags << "V"
    @filter_tags << "W"
    @filter_tags << "X"
    @filter_tags << "Y"
    @filter_tags << "Z"
    @filter_tags << "Ž"
  end

  def index
    if params.include? :filter_tag
      tag = params[:filter_tag]
      if @filter_tags.include? tag
        documents_index(params, false, '(title collate "cz")', tag)
      end
    else
      documents_index(params)
    end
  end

  def show
    document_show(Term, params)
  end

  def drafts
    documents_index(params, drafts=true)
    @breadcrumbs << Breadcrumb.new(t :list_drafts)
    render :index
  end

  def new
    @doc = Term.new
    @current_category = current_category
    @breadcrumbs = []
  end

  def create
    doc = create_new_document!(Term, params)
    flash[:notice] = t(:document_created)
    redirect_to edit_term_path(doc)
  end

  def edit
    @doc = Term.find params[:id]
    @current_category = current_category
    @breadcrumbs = []
  end

  def update
    doc = Term.find(params[:id])
    update_document!(doc, params)
    flash[:notice] = t(:document_saved)

    redirect_to edit_term_path(doc)
  end

  def publish
    doc = Term.find params[:id]
    doc.is_draft = false
    doc.save!
    flash[:notice] = t(:document_published, :name => doc.title)
    redirect_to term_path(doc)
  end

  def unpublish
    doc = Term.find params[:id]
    doc.is_draft = true
    doc.save!
    flash[:notice] = t(:document_unpublished, :name => doc.title)
    redirect_to terms_drafts_path
  end

  def destroy
    doc = Term.find params[:id]
    doc.destroy
    redirect_to terms_path
  end
end
