// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require underscore
//= require lemmon-slider
//= require jquery-ui-1.8.23.custom
//= require galleria-1.6.1.min.js
//= require summernote-lite.min.js
//= require summernote-cs-CZ.js
//= require_tree .

function showSeznamMap () {
	// Only show the map if the element is not there
	if (document.getElementById("locationmap") == null) {
		return;
	}

	function layer(layerName) {
		const API_KEY = "cS6jz2OZdcB4x3Q41nf_igoSLwqXsZHz52kmgsT3Qvg";
		return "https://api.mapy.cz/v1/maptiles/" + layerName + "/256/{z}/{x}/{y}?apikey=" + API_KEY
	}

	const attribution = '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>';
	var position;
	var zoom;
	var map;

    if (typeof egeonLatitude !== "undefined" && typeof egeonLongitude !== "undefined" && typeof egeonMarkerTitle !== "undefined" && typeof egeonInfoWindowContents !== "undefined") {
		position = [egeonLatitude, egeonLongitude];
		zoom = 13;
		map = L.map('locationmap').setView(position, zoom);

		var marker = L.marker([egeonLatitude, egeonLongitude], {
			"title": egeonMarkerTitle,
		});
		marker.bindPopup(egeonInfoWindowContents);
		marker.addTo(map);
	} else if (typeof egeonMapMarkers !== "undefined") {
		position = [50, 14];
		zoom = 6;
		map = L.map('locationmap').setView(position, zoom);

		console.log("Loading map markers...");
		$.getJSON("/mapy/markers.json", function(egeonMapMarkers, status) {
			if(status !== "success") {
				console.log("Error loading map markers: unexpected status: '" + status + "'");
				return;
			}

			var markers = L.markerClusterGroup();

			var item = null;
			var markerIndex;
			for(markerIndex = 0; markerIndex < egeonMapMarkers.length; markerIndex++) {
				item = egeonMapMarkers[markerIndex];

				var icon = L.icon({
					iconUrl: item.picture,
				});

				var marker = L.marker([item.lat, item.lng], {
					title: item.title,
					icon: icon,
				});
				marker.bindPopup(item.description);
				markers.addLayer(marker);
			}

			map.addLayer(markers);

			console.log("Finished loading all the markers.");
		});
	} else {
		// No map coordinates or markers were provided
	}


	const layerOptions = {
		minZoom: 0,
		maxZoom: 19,
		attribution: attribution,
	};

	const layers = {
		"Základní": L.tileLayer(layer("basic"), layerOptions),
		"Turistická": L.tileLayer(layer("outdoor"), layerOptions),
		"Letecká": L.tileLayer(layer("aerial"), layerOptions),
	};

	layers["Turistická"].addTo(map);

	L.control.layers(layers).addTo(map);

	// Include the mapy.cz logo on the page (this is a requirement):
	const LogoControl = L.Control.extend({
		options: {
			position: 'bottomleft',
		},

		onAdd: function (map) {
			const container = L.DomUtil.create('div');
			const link = L.DomUtil.create('a', '', container);

			link.setAttribute('href', "https://mapy.cz/turisticka?x=" + position[1] + "&y=" + position[0] + "&z=" + zoom);
			link.setAttribute('target', '_blank');
			link.innerHTML = '<img src="https://api.mapy.cz/img/api/logo.svg" />';
			L.DomEvent.disableClickPropagation(link);

			return container;
		},
	});

	new LogoControl().addTo(map);
}



function deprecatedShowSeznamMap () {
  var position, map, marker;
  if (typeof egeonLatitude !== "undefined" && typeof egeonLongitude !== "undefined" && typeof egeonMarkerTitle !== "undefined" && typeof egeonInfoWindowContents !== "undefined") {
    var position = SMap.Coords.fromWGS84(egeonLongitude, egeonLatitude);
    var map = new SMap(JAK.gel("locationmap"), position, 13);
    map.addDefaultLayer(SMap.DEF_TURIST).enable();
    map.addDefaultControls();
    var layer = new SMap.Layer.Marker();
    var marker = new SMap.Marker(position);
    layer.addMarker(marker);
    map.addLayer(layer);
    layer.enable();
  } else if (typeof egeonMapMarkers !== "undefined") {
    var position = SMap.Coords.fromWGS84(14, 50);
    var map = new SMap(JAK.gel("locationmap"), position, 6);
    map.addDefaultLayer(SMap.DEF_TURIST).enable();
    map.addDefaultControls();

    var layer = new SMap.Layer.Marker();
    var clusterer = new SMap.Marker.Clusterer(map);
    layer.setClusterer(clusterer);
    var markers = [];

    $.getJSON("/mapy/markers.json", function(egeonMapMarkers, status) {
      if(status !== "success") {
        console.log("Error loading map markers: unexpected status: '" + status + "'");
        return;
      }
      var item = null;
      var markerIndex;
      var markers = [];
      for(markerIndex = 0; markerIndex < egeonMapMarkers.length; markerIndex++) {
        item = egeonMapMarkers[markerIndex];

        var card = new SMap.Card();
        card.getHeader().innerHTML = "<strong>" + item.title + "</strong>";
        card.getBody().innerHTML = item.description;
        var marker = new SMap.Marker(SMap.Coords.fromWGS84(item.lng, item.lat), null, {url: item.picture});
        marker.decorate(SMap.Marker.Feature.Card, card);
        markers.push(marker);
      }

      layer.addMarker(markers);
      map.addLayer(layer);
      layer.enable();
      console.log("Finished loading all the markers.");
    });
  } else {
    // No map coordinates or markers were provided
  }
}
