window.console = {log: () ->} unless window.console?


setUnsavedDataWarning = () ->
  window.onbeforeunload = () ->
    return 'Stránka obsahuje neuložená data.'


isDocumentFormPage = () ->
  $('textarea.richtext').length > 0

replaceName = ($elem, regex, newName) ->
  name = $elem.prop("name")
  if name
    $elem.prop "name", name.replace(regex, newName)
    $elem.prop "id", name.replace(regex, newName)


galleryPhotoStripe = ->
  $gallery = $(".photos .gallery")
  return if $gallery.length is 0
  $gallery.lemmonSlider loop: false


$("button.delete.photo").live "click", (e) ->
  e.preventDefault()
  $(this).parents("li").slideUp ->
    $deletion_marker = $(this).find("input[rel=\"delete\"]")
    $(this).empty()
    $deletion_marker.val 1
    $(this).append $deletion_marker


$("button#today").live "click", (e) ->
  e.preventDefault()
  today = new Date()
  dateStr = (today.getDate() + ". " + (today.getMonth() + 1) + ". " + today.getFullYear())
  $("#destination_last_checkup").val dateStr


$("button#yesterday").live "click", (e) ->
  e.preventDefault()
  d = new Date()
  d.setDate d.getDate() - 1
  dateStr = d.getDate() + ". " + (d.getMonth() + 1) + ". " + d.getFullYear()
  $("#destination_last_checkup").val dateStr


$("#more-photos").live "click", (e) ->
  e.preventDefault()
  filePickers = []
  i = 0

  while i < 5
    filePickers.push "<input name=\"new-photos[]\" type=\"file\" />"
    i++
  $newElems = $(filePickers.join(""))
  $newElems.hide()
  $("#more-photos").before $newElems
  $newElems.fadeIn()


$("table + button.add-more").live "click", (e) ->
  e.preventDefault()
  $table = $(this).prev("table")
  $rows = $table.find("tr")
  $row = $rows.last().clone()
  $row.find("input").val ""
  $row.hide()
  i = 0
  while i < 4
    $newrow = $row.clone()
    $newrow.appendTo $rows.parent()
    i++
  $table.find("tr:hidden").fadeIn()

$("button.add-more").live "click", (e) ->
  # TODO
  e.preventDefault()


$("#show-hidden").live "click", (e) ->
  $(this).parent().find(".hidden").slideToggle()
  $(this).find(".ellipsis").toggle()


isChecked = (checkbox) ->
  $(checkbox).is ":checked"

hookLodgingInteraction = ->
  # Show/Hide the lodging form depending on the selected categories
  $lodgingCheckboxes = $("#categories_3866, #categories_2714, #categories_4769")
  $lodgingCheckboxes.change ->
    $("#lodging").toggle _.any($lodgingCheckboxes, isChecked)

  $("#lodging").toggle _.any($lodgingCheckboxes, isChecked)

  # Change the room description text when camps category is selected
  $camps_cb = $("#categories_2714")
  $camps_cb.change ->
    $l = $('label[for="lodging_room_description"]')
    if isChecked($(this))
      $l.text($l.data('camps'))
    else
      $l.text($l.data('normal'))

recalculateRouteSectionDistanceSums = () ->
  distanceSums = []
  $('#route_section_entries input[rel="distance"]').each (ix, el) ->
    distance = parseFloat($(el).val())
    distance = 0 unless _.isFinite(distance)
    distanceSums[ix] = distance
  $('#route_section_entries input[rel="distance_sum"]').each (ix, el) ->
    sum = _.reduce distanceSums[0..ix], ((a, b) -> a + b), 0
    sum = '' unless sum > 0
    $(el).val(sum)



$('input[type="submit"]').live 'click', (e) ->
  window.onbeforeunload = null


# Things that require DOM without the images loaded go here
$(document).ready ->
  $("#categories ul").hide()
  # show the top-level categories
  $("#categories").children("ul").show()
  # show categories that contain selected subcategories
  $("#categories").find('ul').each (index, ul) ->
    $ul = $(ul)
    if $ul.children('li').children('input:checked').length > 0
      $ul.show()

  # Select all children
  $("#categories input").change ->
    checked = $(this).is(":checked")
    $parentCheckboxes = $(this).parents("ul").siblings("input")
    if checked
      $(this).siblings("ul").show()
      $parentCheckboxes.prop "checked", true
    else
      $childrenCheckboxes = $(this).siblings("ul").children("li").children("input")
      $childrenCheckboxes.filter(":checked").each ->
        $(this).click()

      $(this).siblings("ul").hide()  if $childrenCheckboxes.filter(":checked").length is 0

  hookLodgingInteraction()

  if isDocumentFormPage()
    $('input').live('keydown', setUnsavedDataWarning)
    $('textarea').live('keydown', setUnsavedDataWarning)
    $('.note-editor').live('keydown', setUnsavedDataWarning)
    $("button.delete.photo").live('click', setUnsavedDataWarning)
    $('#route_section_entries input[rel="distance"]').keyup () ->
     _.defer(recalculateRouteSectionDistanceSums)
    recalculateRouteSectionDistanceSums()

    $('.richtext').summernote({
      lang: 'cs-CZ'
      toolbar: [
        ['style', ['bold', 'italic', 'underline']],
        ['font', ['superscript', 'subscript']],
        ['insert', ['link']]
      ]
    })

  if $('#galleria').length > 0
    Galleria.loadTheme('/galleria/classic/galleria.classic.min-1.6.1.js')
    Galleria.configure({
      height: 300
      lightbox: true
      showInfo: false
    })
    Galleria.run()
    Galleria.ready () ->
      $galleria = this

      prevHandler = (e) ->
        first = $galleria.getIndex() is 0
        if first
          e.preventDefault()
        else
          $galleria.prev()
      nextHandler = (e) ->
        last = $galleria.getIndex()+1 is $galleria.getDataLength()
        if last
          e.preventDefault()
        else
          $galleria.next()

      $('.galleria-image-nav-left')
        .unbind()
        .on 'mousedown', prevHandler
      $('.galleria-image-nav-right')
        .unbind()
        .on 'mousedown', nextHandler

      this.bind 'lightbox_image', (e) ->
        index = this._lightbox.active
        first = index is 0
        $('.galleria-lightbox-prevholder').toggle(!first)
        last = index+1 is $galleria.getDataLength()
        $('.galleria-lightbox-nextholder').toggle(!last)


# This gets executed after everything including images is loaded
$(window).load ->
  galleryPhotoStripe()
  $('#edit-photos ul').sortable({tolerance: 'pointer', handle: '.drag-handle'})
  showSeznamMap();