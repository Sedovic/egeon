module ApplicationHelper
  def current_section
    cat = Category.find_by_name(controller.controller_name)
    return cat if cat && cat.root?
  end

  def category_path(category)
    return nil unless category.respond_to? :root?

    if action_name == 'drafts'
      return "/#{category.top_section.name}/drafts?c=#{category.id}"
    end

    if category.root?
      "/#{t 'url_routing.' + category.name}/"
    else
      "/#{t 'url_routing.' + category.top_section.name}/#{category.name}/"
    end
  end

  def edit_photos(photos)
    return render :partial => '/edit_photos', :locals => { :photos => photos }
  end

  def render_category_checkboxes(categories, selected=[])
    return render :partial => '/category_checkboxes', :locals => {
      :categories => categories,
      :selected => selected,
    }
  end

  def render_description(text)
    # TODO: scrub this for improper HTML
    return text
  end

  def document_show_url(doc, category_name=nil)
    category = Category.find_by_name(category_name)
    category_query = if category
      "?c=#{category.id}"
    else
      ""
    end
    "/#{I18n::t 'url_routing.' + doc.top_level_section.name}/#{doc.id}/#{doc.normalized_title}#{category_query}"
  end

  def show_document_or_edit_draft_link(doc, category_name=nil)
    url = if doc.draft?
            edit_polymorphic_path(doc)
          else
            document_show_url(doc, category_name)
          end
    link_to doc.title, url, :rel => 'document'
  end

  def normalize_url(url)
    return if url.blank?

    if url =~/\Ahttps?:\/\//
      url
    else
      "http://#{url}"
    end
  end

  def document_thumbnail(target, photos, motivation=nil)
    render :partial => '/document_thumbnail', :locals => {
      :target => target,
      :photos => photos,
      :motivation => motivation,
    }
  end

  def date_and_place(date, place)
    result = [date, place].map(&:to_s).map(&:strip).select(&:present?).join(', ')
    if result.present?
      result
    else
      t('not_available')
    end
  end
end
