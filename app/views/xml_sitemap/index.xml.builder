xml.instruct! :xml, :version=>'1.0'

xml.tag! 'urlset', "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do

  @sitemap_urls.each do |sitemap_url|
    xml.url do
      xml.loc sitemap_url.location
      xml.changefreq sitemap_url.change_frequency
      xml.priority sitemap_url.priority
    end
  end

end