def click_action_link(action_name)
  action = action_name.to_s
  find("a[rel~=#{action}]").click
end

def visit_section(section_name)
  section = section_name.to_s
  click_action_link section
end

def verify_confirmation_message
  page.should have_css '.alert.alert-info'
end

def save_document
  find('input[name="save"]').click
end


def create_destination_document(title)
  visit_section :destinations
  click_on('new_document')
  page.should have_css 'form#new_destination'
  fill_in 'destination_title', :with => title
  check 'categories_4781'
  save_document
  verify_confirmation_message
end

def create_route_document(title)
  visit_section :routes
  click_on('new_document')
  page.should have_css 'form#new_route'
  fill_in 'route_title', :with => title
  check 'categories_4592'
  save_document
  verify_confirmation_message
end

def create_event_document(title)
  visit_section :events
  click_on('new_document')
  page.should have_css 'form#new_event'
  fill_in 'event_title', :with => title
  check 'categories_5939'
  save_document
  verify_confirmation_message
end

def create_term_document(title)
  visit_section :terms
  click_on('new_document')
  page.should have_css 'form#new_term'
  fill_in 'term_title', :with => title
  save_document
  verify_confirmation_message
end

def create_personality_document(name, surname)
  visit_section :personalities
  click_on('new_document')
  page.should have_css 'form#new_personality'
  fill_in 'personality_name', :with => name
  fill_in 'personality_surname', :with => surname
  save_document
  verify_confirmation_message
end
