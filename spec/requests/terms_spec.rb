require 'spec_helper'
require 'navigation_helper'

describe "terms" do
  before(:each) do
    visit('/')
    fill_in 'user_email', :with => 'jack@example.com'
    fill_in 'user_password', :with => 'password'
    click_on 'log_in'
    page.should have_content 'Jack Doe'
  end

  it "can be created by editors" do
    create_term_document('Test document')
    click_action_link :drafts
    page.should have_content 'Test document'
  end

  it "can be deleted by editors" do
    count = Term.count
    create_term_document('This doc should be deleted')
    Term.count.should eq(count+1)
    click_action_link :drafts
    page.should have_content 'This doc should be deleted'
    find('a', :text => 'This doc should be deleted').click
    click_action_link :delete
    click_action_link :drafts
    page.should_not have_content 'This doc should be deleted'
    visit_section :terms
    page.should_not have_content 'This doc should be deleted'
    Term.count.should eq(count)
  end

  it "can be edited" do
    create_term_document 'A document to be edited'
    click_action_link :drafts
    find('a', :text => 'A document to be edited').click
    page.should have_css 'form.edit_term'
    fill_in 'term_title', :with => 'This doc been edited'
    fill_in 'term_description', :with => 'And it has a description now'
    save_document
    verify_confirmation_message
    click_action_link :drafts
    page.should have_content 'This doc been edited'
    page.should_not have_content 'A document to be edited'
  end

  it "can be published and unpublished" do
    create_term_document 'A document to be published'
    click_action_link :drafts
    find('a', :text => 'A document to be published').click
    click_action_link :publish
    verify_confirmation_message
    click_action_link :drafts
    page.should_not have_content 'A document to be published'
    visit_section :terms
    page.should have_content 'A document to be published'
    find('a', :text => 'A document to be published').click
    page.should have_content 'A document to be published'

    click_action_link :unpublish
    verify_confirmation_message
    click_action_link :drafts
    page.should have_content 'A document to be published'
    visit_section :terms
    page.should_not have_content 'A document to be published'
  end


  it "can have photos attached to them", :js => true do
    visit_section :terms
    click_on('new_document')
    page.should have_css 'form#new_term'
    fill_in 'term_title', :with => 'A document with photos'
    attach_file 'new_photo_1', "#{File.dirname(__FILE__)}/../test-data/photo-1.jpg"
    attach_file 'new_photo_2', "#{File.dirname(__FILE__)}/../test-data/photo-2.jpg"
    attach_file 'new_photo_3', "#{File.dirname(__FILE__)}/../test-data/photo-3.jpg"
    attach_file 'new_photo_4', "#{File.dirname(__FILE__)}/../test-data/photo-4.jpg"
    attach_file 'new_photo_5', "#{File.dirname(__FILE__)}/../test-data/photo-5.jpg"
    save_document
    verify_confirmation_message
    click_action_link :publish
    visit_section :terms
    find('a', :text => 'A document with photos').click
    all('.photos .gallery a img').count.should eq(5)

    click_action_link :edit
    all('#edit-photos a img').count.should eq(5)
    attach_file 'new_photo_1', "#{File.dirname(__FILE__)}/../test-data/photo-6.jpg"
    attach_file 'new_photo_2', "#{File.dirname(__FILE__)}/../test-data/photo-7.jpg"
    save_document
    verify_confirmation_message
    visit_section :terms
    find('a', :text => 'A document with photos').click
    all('.photos .gallery a img').count.should eq(7)

    click_action_link :edit
    all('#edit-photos a img').count.should eq(7)
    find('#edit-photos button.delete.photo').click
    find('input[rel="delete"][value="1"]')  # wait for the deletion JS action to take effect
    save_document
    verify_confirmation_message
    all('#edit-photos a img').count.should eq(6)
    visit_section :terms
    find('a', :text => 'A document with photos').click
    all('.photos .gallery a img').count.should eq(6)
  end

  it "display searched-for terms" do
    visit_section :terms
    fill_in 'search', :with => 'SomeNonExistentSearchTerm'
    find('#search_button').click
    all('a[rel="document"]').count.should eq(0)
    page.should have_css '.not-found'

    Term.create!(:title => 'DocumentForSearch', :author => User.first, :categories => [])
    ThinkingSphinx::Test.index; sleep(0.5)
    fill_in 'search', :with => 'DocumentForSearch'
    find('#search_button').click
    all('a[rel="document"]').count.should eq(1)
    page.should_not have_css '.not-found'
  end
end