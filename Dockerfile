# NOTE: It's no longer possible to build the regular egeon container
# from scratch. A gem deep within the dependency hierarchy was yanked,
# which in rubygems parlance means deleted on the remote server and
# therefore `bundle install` fails.
#
# Updating the dependencies means updating rails and entering the
# dependency hell really quickly. We need to do that, but it'll take a
# massive amount of time.
#
# For now, let's just use the production container as the source and
# just update the Rails egeon code. This will work until we need to
# change dependencies.

FROM quay.io/tomassedovic/egeon:1.0.1

WORKDIR /var/lib/egeon/
ENV HOME /var/lib/egeon/
USER egeon

ADD Gemfile Gemfile.lock /var/lib/egeon/

# Script to set up the database
ADD run_migrations.sh /run_migrations.sh

# Copy the app stuff to the container:
ADD --chown=egeon:egeon . /var/lib/egeon/

ENV RAILS_ENV production

EXPOSE 3000
