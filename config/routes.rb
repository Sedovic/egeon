Geoinfotour::Application.routes.draw do
  devise_for :users

  root :to => redirect('/' + I18n::t('url_routing.destinations') + '/')

  sections = ['destinations', 'routes', 'events', 'terms', 'personalities']
  sections.each do |section|
    match("#{section}/" => "#{section}#index",
          :defaults => { :category_name => "#{section}"}, :via => :get)
    match("#{section}/:id" => "#{section}#show",
          :constraints => {:id => /\d+/}, :via => :get)
    match("#{section}/:id/publish" => "#{section}#publish",
          :constraints => {:id => /\d+/}, :via => :post, :as => "publish_#{section.singularize}")
    match("#{section}/:id/unpublish" => "#{section}#unpublish",
          :constraints => {:id => /\d+/}, :via => :post, :as => "unpublish_#{section.singularize}")
    match "#{section}/new" => "#{section}#new", :via => :get
    match "#{section}/drafts" => "#{section}#drafts", :via => :get
    match "#{section}/:category_name" => "#{section}#index", :via => :get
    resources section
  end

  resources 'pages'

  sections.each do |section|
    match(I18n::t("url_routing.#{section}") + '/' => "#{section}#index",
          :defaults => { :category_name => "#{section}"}, :via => :get)
    match("#{I18n::t 'url_routing.' + section}/:id(/:title)" => "#{section}#show",
          :constraints => {:id => /\d+/}, :via => :get)
    match "#{I18n::t 'url_routing.' + section}/:category_name" => "#{section}#index", :via => :get
  end

  match("#{I18n::t("url_routing.maps")}/" => "maps#index", :via => :get)
  match("#{I18n::t("url_routing.maps")}/markers" => "maps#markers", :via => :get)

  match("#{I18n::t("url_routing.about")}/" => "about#index", :via => :get)
  match("#{I18n::t("url_routing.about")}/edit" => "about#edit", :via => :get)

  match("#{I18n::t("url_routing.sitemap")}/" => "sitemap#index", :via => :get)

  match("/sitemap.:format" => "xml_sitemap#index", :via => :get)

  resources :users

end
