#!/usr/bin/env ruby

require 'net/http'
require 'mechanize'

APP_URL = ENV['APP_URL']
EMAIL = ENV['EMAIL']
PASSWORD = ENV['PASSWORD']

raise Exception, 'You must specify APP_URL environment variable!' unless APP_URL
raise Exception, 'You must specify EMAIL environment variable!' unless EMAIL
raise Exception, 'You must specify PASSWORD environment variable!' unless PASSWORD


def alert?(page)
  !page.search(".alert").empty? && page.search(".alert-info").empty?
end

def success?(page)
  !page.search(".alert-info").empty?
end

a = Mechanize.new

# Test home page
page = a.get(APP_URL)


# Test login page
login_uri = URI(APP_URL)
login_uri.path = '/users/sign_in'
login_page = a.get login_uri
login_form = login_page.forms.first
login_form['user[email]'] = EMAIL
login_form['user[password]'] = PASSWORD
page = a.submit(login_form)

if alert?(page)
  pp page
  raise Exception, "Login failed"
end

if page.search('#categories-pane').empty?
  pp page
  raise Exception, "Couldn't find the Categories pane"
end


# Test Search
page = a.get(APP_URL)
page = page.links.find {|l| l.rel.include? 'destinations'}.click
search_form = page.forms.first
search_form.q = "Adamov"
page = a.submit(search_form)

if page.search('#content a[rel="document"]').empty?
  pp page
  raise Exception, "Search didn't find any Adamov links (it should have!)"
end


# Test creating new destination
page = a.get(APP_URL)
page = page.links.find {|l| l.rel.include? 'personalities'}.click
page = page.link_with(:id => 'new_document').click

if page.search('form#new_personality').empty?
  pp page
  raise Exception, "Clicking New Document didn't get us to the new doc page"
end

form = page.forms.first
form['personality[name]'] = "zztestjohn"
form['personality[surname]'] = "zztestdoe"
form['personality[description]'] = "somedescription"
page = a.submit(form)

unless success?(page)
  pp page
  raise Exception, "Creating a new document didn't show the success alert"
end

# The unobtrusive javascript shit doesn't work with Mechanize so we've added
# a form with _method override to automate document deletion:
page = a.submit(page.form('delete_document'))

if page.search('#content a[rel="document"]').empty?
  pp page
  raise Exception, "Didn't get redirected back to the documents list after deletion"
end
