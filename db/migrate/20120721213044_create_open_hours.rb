class CreateOpenHours < ActiveRecord::Migration
  def change
    create_table :open_hours do |t|
      t.integer :destination_id, :null => false

      t.string :season
      t.string :timespan
      t.string :notes
    end
  end
end
