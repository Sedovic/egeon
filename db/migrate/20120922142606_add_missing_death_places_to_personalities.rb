# encoding: utf-8
class AddMissingDeathPlacesToPersonalities < ActiveRecord::Migration

  def missing_death_places
    [
      {:name=>"Vatroslav", :surname=>"Jagič", :deathplace=>"Wien"},
      {:name=>"Josef", :surname=>"Pelíšek", :deathplace=>"Brno"},
      {:name=>"Rudolf", :surname=>"Luskač", :deathplace=>"Praha"},
      {:name=>"Jan ", :surname=>"Knies", :deathplace=>"Brno"},
      {:name=>"Hugo ", :surname=>"Sáňka", :deathplace=>"Brno"},
      {:name=>"Otakar ", :surname=>"Chlup", :deathplace=>"Praha"},
      {:name=>"Klement", :surname=>"Bochořák", :deathplace=>"Brno"},
      {:name=>"Ferdinand", :surname=>"Saar", :deathplace=>"Wien"},
      {:name=>"Josef ", :surname=>"Sekanina", :deathplace=>"Letovice"},
      {:name=>"Bedřich František", :surname=>"Ševčík", :deathplace=>"Wien"},
      {:name=>"Josef", :surname=>"Augusta", :deathplace=>"Praha"},
      {:name=>"Josef", :surname=>"Uher", :deathplace=>"Brno"},
      {:name=>"Karel ", :surname=>"Absolon", :deathplace=>"Brno"},
      {:name=>"Jindřich", :surname=>"Wankel", :deathplace=>"Olomouc"},
      {:name=>"Karla", :surname=>"Bufková-Wanklová", :deathplace=>"Praha"},
      {:name=>"Hugo Václav ", :surname=>"Sáňka", :deathplace=>"Blansko"},
      {:name=>"Christoph", :surname=>"Liebich", :deathplace=>"Praha"},
      {:name=>"Rudolf", :surname=>"Haša", :deathplace=>"Vsetín"},
      {:name=>"Stanislav Kostka", :surname=>"Neumann", :deathplace=>"Praha"},
      {:name=>"Augustin", :surname=>"Berger", :deathplace=>"Praha"},
      {:name=>"Karel Jaroslav", :surname=>"Maška", :deathplace=>"Brno"},
      {:name=>"Jan Nepomuk ", :surname=>"Soukop", :deathplace=>"Doubravice nad Svitavou"},
      {:name=>"Pankrác", :surname=>"Krkoška", :deathplace=>"Brno"},
      {:name=>"František", :surname=>"Halas", :deathplace=>"Praha"},
      {:name=>"Karel Hynek", :surname=>"Mácha", :deathplace=>"Litoměřice"},
      {:name=>"Jan ", :surname=>"Kunc", :deathplace=>"Brno"},
      {:name=>"Johannes Anton", :surname=>"Nagel", :deathplace=>"Wien"},
      {:name=>"Jan Blažej ", :surname=>"Santini-Aichel", :deathplace=>"Praha"},
      {:name=>"Ludvík ", :surname=>"Kundera", :deathplace=>"Boskovice"},
      {:name=>"Alois ", :surname=>"Slovák", :deathplace=>"Brno"},
      {:name=>"Hugo František", :surname=>"Salm-Reifferscheidt", :deathplace=>"Wien"},
      {:name=>"Rudolf", :surname=>"Těsnohlídek", :deathplace=>"Brno"},
      {:name=>"Alois", :surname=>"Král", :deathplace=>"Tišnov"},
      {:name=>"Josef", :surname=>"Ressel", :deathplace=>"Ljubljana (Slovinsko)"},
      {:name=>"Karel", :surname=>"Svěrák", :deathplace=>"Mauthausen"},
      {:name=>"Karl ", :surname=>"Reichenbach", :deathplace=>"Leipzig (Lipsko)"},
      {:name=>"Jan ", :surname=>"Skutil", :deathplace=>"Brno"},
      {:name=>"Alois Vojtěch ", :surname=>"Šembera", :deathplace=>"Wien"},
      {:name=>"Stanislav", :surname=>"Lolek", :deathplace=>"Uherské Hradiště"},
      {:name=>"Rudolf", :surname=>"Burkhardt", :deathplace=>"Brno"},
      {:name=>"Josef", :surname=>"Opletal", :deathplace=>"Brno"},
    ]
  end

  def up
    missing_death_places.each do |attributes|
      personality = Personality.find_by_name_and_surname(attributes[:name], attributes[:surname])
      if personality.present?
        personality.deathplace = attributes[:deathplace]
        personality.name.strip!
        personality.surname.strip!
        personality.save!
      end
    end
  end

  def down
  end
end
