class MoveAllDocsToDrafts < ActiveRecord::Migration

  def move_to_drafts(doc)
    unless doc.draft?
      puts "Migrating #{doc.id} '#{doc.title}' to drafts"
      doc.is_draft = true
      doc.save!
    end
  end

  def up
    Destination.all.each { |d| move_to_drafts(d) }
    Route.all.each { |d| move_to_drafts(d) }
    Event.all.each { |d| move_to_drafts(d) }
    Term.all.each { |d| move_to_drafts(d) }
    Personality.all.each { |d| move_to_drafts(d) }
  end

  def down
  end
end
