class CreateLodgings < ActiveRecord::Migration
  def change
    create_table :lodgings do |t|
      t.integer :destination_id, :null => false

      t.text :general_notes
      t.text :room_description
      t.text :services_included
      t.text :services_available
      t.text :internet_availability
      t.text :arrival_since
      t.text :departure_till
      t.text :children_extra_beds
      t.text :animals
      t.text :bookings_payments_cancellation
      t.boolean :accept_credit_cards
    end
  end
end
