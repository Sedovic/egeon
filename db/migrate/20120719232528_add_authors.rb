class AddAuthors < ActiveRecord::Migration
  def change
    add_column :destinations, :author_id, :integer
    add_column :events, :author_id, :integer
    add_column :personalities, :author_id, :integer
    add_column :routes, :author_id, :integer
    add_column :terms, :author_id, :integer

    create_table :collaborations do |t|
      t.references :document, :polymorphic => true
      t.integer :user_id, :null => false
    end
  end
end
