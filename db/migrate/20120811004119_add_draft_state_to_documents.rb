class AddDraftStateToDocuments < ActiveRecord::Migration
  def change
    change_table :destinations do |t|
      t.boolean :is_draft, :null => false, :default => false
    end
    change_table :events do |t|
      t.boolean :is_draft, :null => false, :default => false
    end
    change_table :personalities do |t|
      t.boolean :is_draft, :null => false, :default => false
    end
    change_table :routes do |t|
      t.boolean :is_draft, :null => false, :default => false
    end
    change_table :terms do |t|
      t.boolean :is_draft, :null => false, :default => false
    end
  end
end
