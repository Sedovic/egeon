class MigrateOldCategoriesToNewCategories < ActiveRecord::Migration
  # def up
  #   # Destination.where(:document_categories => )
  #
  #   # prvni je oldCategoryId a druhe je newCategoryId!
  #   categoriesToRemove = []
  #
  #
  #   # move HOTELY,MOTELY a KEMPY subcats TO UBYTOVANI cat
  #   # odebrat -> presun do nadrazene o 1
  #   categoriesToRemove << 3866
  #   categoriesToRemove << 2714
  #
  #   # move OPRAVNY KOL subcat TO CYKLO cat
  #   # odebrat -> presun do nadrazene o 1
  #   categoriesToRemove << 8851
  #
  #   # move PUDY subcat to MAIN PUDY cat
  #   categoriesToRemove << 8375
  #
  #   # move ZIVOCICHOVE subcats TO MAIN ZIVOCICHOVE
  #   categoriesToRemove << 8363
  #
  #   # move ROSTLINY subcats TO MAIN ROSTLINY
  #   categoriesToRemove << 6324
  #
  #
  #
  #   # categoriesToRemove.each do |categoryId|
  #   #   # execute <<-SQL
  #   #   #   DELETE FROM document_categories
  #   #   #     WHERE category_id = category.
  #   #   # SQL
  #   #
  #   #   destinations = Destination.where(document_categories.include? categoryId)
  #   #
  #   #   destinations.document_categories.delete_all(categoriesToRemove.include? :categoryId)
  #   # end
  #
  #
  #   # Deletes all records from document_categories table with categoryId in categoriesToRemove collection
  #
  #   categoriesToRemove.each do |categoryIdToRemove|
  #     Destination.all.each do |destination|
  #       destination.document_categories.where(category_id: categoryIdToRemove).delete_all
  #
  #       if destination != nil
  #         destination.save!
  #       end
  #     end
  #   end
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #
  #   # prvni je oldCategoryId a druhe je newCategoryId!
  #   changeOldToNewCategory = []
  #
  #
  #   # move VODOPADY to WATER cat
  #   changeOldToNewCategory << [8397, 8358]
  #
  #   ##### u zaznamu s ID 8397 zmenit zaznam s ID 4316 na 8358 /ztrata dat
  #   ##### ---------> eventualne se da nahradit za presun z ID 8397 na 8358, cimz se ale ztrati zbytecne detail
  #
  #
  #   # move VENKOVSKA SIDLA CONTENT to SIDLA cat
  #   # zmenit -> zmena na jinou ve stejne urovni
  #   changeOldToNewCategory << [3549, 9834]
  #
  #   # add subcats to new KRAS cat
  #   changeOldToNewCategory << [6963, 1113]
  #   changeOldToNewCategory << [8408, 1113]
  #   changeOldToNewCategory << [8395, 1113]
  #   changeOldToNewCategory << [4309, 1113]
  #
  #   # add subcats to new LOMY cat
  #   changeOldToNewCategory << [2251, 1114]
  #   changeOldToNewCategory << [4297, 1114]
  #
  #   # move KOSTELY, KAPLE subcats TO KOSTELY A KAPLE
  #   changeOldToNewCategory << [8802, 1116]
  #   changeOldToNewCategory << [2655, 1116]
  #
  #   # move BOZI MUKA, KRIZOVE CESTY subcats TO BOZI MUKA A KRIZE
  #   changeOldToNewCategory << [8796, 1117]
  #   changeOldToNewCategory << [6747, 1117]
  #
  #   # move ZIDOVKE HRBITOVY, SYNAGOGY subcats TO ZIDOVSKE PAMATKY
  #   changeOldToNewCategory << [2639, 1118]
  #   changeOldToNewCategory << [4691, 1118]
  #
  #   # move ZELEZNICNI TRATE, ZELEZNICNI STANICE ZASTAVKY subcats TO ZELEZNICE
  #   changeOldToNewCategory << [9303, 1119]
  #   changeOldToNewCategory << [2570, 1119]
  #
  #   ##### zmenit zaznamy s ID 9303, 2570 na 1119 /ztrata dat
  #
  #   # move OSOBNI PRISTAVY, NAKLADNI PRISTAVY, TRAJEKTOVE PRISTAVY subcats TO PRISTAVY
  #   changeOldToNewCategory << [9087, 1120]
  #   changeOldToNewCategory << [6653, 1120]
  #   changeOldToNewCategory << [8700, 1120]
  #
  #   # move ALL OLD subcats TO JINE TECH PAMATKY
  #   changeOldToNewCategory << [6498, 4451]
  #   changeOldToNewCategory << [6497, 4451]
  #   changeOldToNewCategory << [2399, 4451]
  #   changeOldToNewCategory << [9627, 4451]
  #   changeOldToNewCategory << [8537, 4451]
  #   changeOldToNewCategory << [4436, 4451]
  #   changeOldToNewCategory << [4434, 4451]
  #   changeOldToNewCategory << [8529, 4451]
  #   changeOldToNewCategory << [4428, 4451]
  #   changeOldToNewCategory << [2379, 4451]
  #   changeOldToNewCategory << [8521, 4451]
  #   changeOldToNewCategory << [5330, 4451]
  #   changeOldToNewCategory << [4423, 4451]
  #   changeOldToNewCategory << [4422, 4451]
  #   changeOldToNewCategory << [4421, 4451]
  #   changeOldToNewCategory << [6455, 4451]
  #
  #
  #
  #
  #   Destination.all.each do |destination|
  #     destination.document_categories.each do |documentCategory|
  #       changeOldToNewCategory.each do |tuple|
  #         if documentCategory.category_id == tuple[0]
  #           documentCategory.category_id = tuple[1]
  #           documentCategory.save!
  #         end
  #       end
  #     end
  #   end
  #
  # end

  def up
    execute <<-SQL
      DELETE FROM document_categories WHERE category_id IN (3866, 2714, 8851, 8375, 8363, 6324);

      UPDATE document_categories SET category_id = 8358 WHERE category_id = 8397;
      UPDATE document_categories SET category_id = 9834 WHERE category_id = 3549;
      UPDATE document_categories SET category_id = 1113 WHERE category_id IN (6963, 8408, 8395, 4309);
      UPDATE document_categories SET category_id = 1114 WHERE category_id IN (2251, 4297);
      UPDATE document_categories SET category_id = 1116 WHERE category_id IN (8802, 2655);
      UPDATE document_categories SET category_id = 1117 WHERE category_id IN (8796, 6747);
      UPDATE document_categories SET category_id = 1118 WHERE category_id IN (2639, 4691);
      UPDATE document_categories SET category_id = 1119 WHERE category_id IN (9303, 2570);
      UPDATE document_categories SET category_id = 1120 WHERE category_id IN (9087, 6653, 8700);
      UPDATE document_categories SET category_id = 4451 WHERE category_id IN (6498, 6497, 2399, 9627, 8537, 4436, 4434, 8529, 4428, 2379, 8521, 5330, 4423, 4422, 4421, 6455);
      UPDATE document_categories SET category_id = 1141 WHERE category_id IN (2476, 4522, 2473);
      UPDATE document_categories SET category_id = 1142 WHERE category_id IN (4507, 2446);
      UPDATE document_categories SET category_id = 1143 WHERE category_id IN (6538, 2441);
      UPDATE document_categories SET category_id = 1144 WHERE category_id IN (2113, 6534);
      UPDATE document_categories SET category_id = 1145 WHERE category_id = 8611;
      UPDATE document_categories SET category_id = 1146 WHERE category_id = 8592;
      UPDATE document_categories SET category_id = 1147 WHERE category_id IN (2446, 8589);
      UPDATE document_categories SET category_id = 1148 WHERE category_id IN (4518, 4508, 4508, 4480, 8578, 8581);
      UPDATE document_categories SET category_id = 1133 WHERE category_id = 6577;
      UPDATE document_categories SET category_id = 1134 WHERE category_id IN (6581, 6580);
      UPDATE document_categories SET category_id = 1135 WHERE category_id IN (8640, 4543);
      UPDATE document_categories SET category_id = 1136 WHERE category_id = 4531;
      UPDATE document_categories SET category_id = 1137 WHERE category_id = 8624;
      UPDATE document_categories SET category_id = 1138 WHERE category_id = 2487;
      UPDATE document_categories SET category_id = 1139 WHERE category_id = 2494;
      UPDATE document_categories SET category_id = 1140 WHERE category_id = 5061;
      UPDATE document_categories SET category_id = 1125 WHERE category_id = 6904;
      UPDATE document_categories SET category_id = 1127 WHERE category_id = 6220;
      UPDATE document_categories SET category_id = 1129 WHERE category_id = 2402;
      UPDATE document_categories SET category_id = 1130 WHERE category_id = 4858;
      UPDATE document_categories SET category_id = 1131 WHERE category_id = 1444;
      UPDATE document_categories SET category_id = 1126 WHERE category_id IN (8667, 9373);
      UPDATE document_categories SET category_id = 1128 WHERE category_id IN (6615, 8662, 9635);
      UPDATE document_categories SET category_id = 1132 WHERE category_id IN (2526, 4562, 8655, 6595);
    SQL
  end

  def down
    # Down migration is not possible because data-detail was lost during Up migration.
  end

end