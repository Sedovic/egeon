class ChangeElevationToString < ActiveRecord::Migration
  def up
    change_column :destinations, :elevation, :string
  end

  def down
    ActiveRecord::IrreversibleMigration
  end
end
