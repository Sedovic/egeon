class CreateDocumentCategories < ActiveRecord::Migration
  def change
    create_table :document_categories do |t|
      t.references :document, :polymorphic => true
      t.integer :category_id, :null => false
    end
  end
end
