class FixDestinationValuesToUseString < ActiveRecord::Migration
  def change
    change_column :destinations, :cadastre, :text
    change_column :destinations, :contact_notes, :text
    change_column :destinations, :municipality, :text
  end
end
