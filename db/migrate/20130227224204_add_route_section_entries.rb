class AddRouteSectionEntries < ActiveRecord::Migration
  def change
    create_table :route_section_entries do |t|
        t.integer :route_id, :null => false

        t.float :distance, :null => false
        t.float :elevation, :null => false
        t.string :section_mark, :null => false
        t.string :map_marks, :null => false
        t.string :next_direction, :null => false
        t.string :bike_accessibility, :null => false
        t.string :notes, :null => false
    end
  end
end
