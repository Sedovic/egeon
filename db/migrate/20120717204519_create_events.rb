class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :motivation
      t.text :description
      t.timestamps
    end
  end
end
