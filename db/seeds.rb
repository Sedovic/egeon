# Add test users if in dev mode

if ['development', 'test'].include? Rails.env
  User.create! :name => 'Jack Doe', :password => 'password', :email => 'jack@example.com'
  User.create! :name => 'Jill Doe', :password => 'password', :email => 'jill@example.com'
  User.create! :name => 'Mr. Smith', :password => 'p@$$w0rd', :email => 'awesome@matrix.in'
end

