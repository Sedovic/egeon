rows = JSON.parse(File.read '/home/thomas/tmp/dump.json')['rows']
docs = rows.map {|r| r['doc']}

def process_hash(hash, whitelist=[])
  result = hash.clone
  hash.each { |key, value| result[key.gsub('-', '_')] = value }
  result['categories'] ||= []
  result['coordinates'] ||= []
  result['coordinates'] = result['coordinates'].join("\n").strip

  unless whitelist.blank?
    result.select! { |key| whitelist.include? key }
  end
  return result
end

def assert(expr)
  raise "assert failure" unless expr
end

def migrate_document(model, doc_hash)
  model.author = User.find_by_email(doc_hash['author'])

  model.created_at = DateTime.parse(doc_hash['created'])
  model.updated_at = DateTime.parse(doc_hash['updated'] || doc_hash['created'])
  assert model.created_at <= model.updated_at

  add_categories(model, doc_hash)
  add_photos(model, doc_hash)


  if model.categories.length == 1
    model.is_draft = true
  end


  # add_drafts(model, doc_hash)
end

def add_photos(model, doc_hash)
  return unless doc_hash.include? 'photos'
  doc_hash['photos'].each do |photo_hash|
    attrs = process_hash(photo_hash)
    p = Photo.new
    p.attribution = (attrs['author'] || '')
    p.description = (attrs['description'] || '')
    p.image = File::open("/home/thomas/tmp/photos/#{attrs['filename']}")
    p.save!
    model.photos << p
  end
end

def add_categories(model, doc_hash)
  model_type = model.class.to_s.tableize
  model.categories << Category.find_by_name(model_type)

  model.update_categories(doc_hash['categories'] || [])
end

def add_drafts(model, doc_hash)
  return if doc_hash['draft'].blank?
  doc_hash = doc_hash['draft']

  cls = model.class
  attrs = process_hash(doc_hash, cls.attribute_names)
  draft = cls.new attrs
  draft.author = model.author
  add_categories(draft, doc_hash)
  add_photos(draft, doc_hash)
  draft.is_draft = true
  draft.source_id = model.id

  # model.draft = draft
  draft.save!
end

docs.select {|d| d['type'] == 'user'}.each do |d|
  u = User.new
  u.name = d['name']
  u.email = d['email']

  # This is a hack to go around pass Device's validation. We'll replace this
  # with the existing bcrypt hash later
  u.password = 'xxxxxxx'
  u.save!
  u.encrypted_password = d['password_hash']
  u.save!
end


def add_lodging(model, doc_hash)
  lodging_categories = [3866, 2714, 4769]
  if(model.categories.map(&:id) & lodging_categories).blank?
    model.lodging = nil
    return
  end

  attrs = process_hash(doc_hash, Lodging.attribute_names)
  l = Lodging.new attrs
  l.accept_credit_cards = false
  l.general_notes = doc_hash['general_lodging_notes']
  l.arrival_since = doc_hash['arival_since']
  model.lodging = l
end

def add_services(model, doc_hash)
  services = doc_hash['pricing'] || []
  services.each do |hash|
    hash['price'] = hash['cost']
    model.services << Service.new(process_hash(hash, Service.attribute_names))
  end
end

def add_open_hours(model, doc_hash)
  open_hours = doc_hash['open_hours'] || []
  open_hours.each do |hash|
    hash['notes'] = '' if hash['notes'].blank?
    hash['timespan'] = hash['time'] || ''
    hash['season'] = hash['day'] || ''
    model.open_hours << OpenHour.new(process_hash(hash, OpenHour.attribute_names))
  end
end

docs.select {|d| d['type'] == 'destination' }.each do |d|
  d.merge!(d['draft']) if d['draft']
  puts "MIGRATING:", d
  attrs = process_hash(d, Destination.attribute_names)
  dest = Destination.new attrs
  migrate_document(dest, d)

  add_open_hours(dest, d)
  add_services(dest, d)
  add_lodging(dest, d)

  dest.save!
end

docs.select {|d| d['type'] == 'event' }.each do |d|
  attrs = process_hash(d, Event.attribute_names)
  ev = Event.new attrs
  migrate_document(ev, d)

  ev.save!
end

docs.select {|d| d['type'] == 'personality' }.each do |d|
  d['born'] = d['birth-date']
  d['died'] = d['death-date']
  d['birthplace'] = d['birth-place']
  d['deathplace'] = d['deathplace']
  attrs = process_hash(d, Personality.attribute_names)
  per = Personality.new attrs
  migrate_document(per, d)

  per.save!
end

docs.select {|d| d['type'] == 'route' }.each do |d|
  attrs = process_hash(d, Route.attribute_names)
  route = Route.new attrs
  migrate_document(route, d)

  route.save!
end

docs.select {|d| d['type'] == 'term' }.each do |d|
  attrs = process_hash(d, Term.attribute_names)
  term = Term.new attrs
  migrate_document(term, d)

  term.save!
end
